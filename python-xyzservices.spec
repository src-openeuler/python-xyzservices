%global _empty_manifest_terminate_build 0
%global pypi_name xyzservices

Name:           python-%{pypi_name}
Version:        2024.9.0
Release:        1
Summary:        Xyzservices is a lightweight library providing a repository of available XYZ services offering raster basemap tiles.
License:        BSD-3-Clause
URL:            https://github.com/geopandas/xyzservices
Source0:        %{url}/archive/%{version}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:  python3-hatchling
BuildRequires:  python3-virtualenv
BuildRequires:  python3-poetry-core
%description
Xyzservices is a lightweight library providing a repository of available XYZ services offering raster basemap tiles.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
Provides:       python-%{pypi_name}
%description -n python3-%{pypi_name}
Xyzservices is a lightweight library providing a repository of available XYZ services offering raster basemap tiles.

%package help
Summary:        Development documents and examples for %{pypi_name}.
Provides:       python3-%{pypi_name}-doc
%description help
Development documents and examples for %{pypi_name}.

%prep
%autosetup -p1 -n %{pypi_name}-%{version}
sed -i '/name/a\version="%{version}",' setup.py

%build
%pyproject_build

%install
%pyproject_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/doclist.lst .

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{python3_sitelib}/xyzservices*.dist-info/
%{python3_sitelib}/xyzservices
%{_datadir}/xyzservices/

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Nov 12 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 2024.9.0-1
- Update version to 2024.9.0
  Adding back NASA's BlueMarble
  Automated update of GeoportailFrance tiles
  Migrate ign wmts services

* Thu Dec 28 2023 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 2023.10.1-1
- Update version to 2023.10.1 and fix package metadata

* Fri Aug 18 2023 zoujiancang<jczou@isoftstone.com> - 2023.7.0-1
- Initial package

